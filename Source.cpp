#include <iostream>
#include "Numsys.h"
#include <string>
using namespace std;

void main()
{
	cout << Numsys::dec_to_hex("124232") << endl;

	cout << Numsys::hex_to_dec("1E548") << endl;

	cout << Numsys::dec_to_oct("125") << endl;

	cout << Numsys::oct_to_dec("175") << endl;

	cout << Numsys::dec_to_bin("58") << endl;

	cout << Numsys::bin_to_dec("111010") << endl;

	system("pause");
}