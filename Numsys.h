#pragma once
#include <iostream>
#include <string>
#pragma warning (disable: 4996)
using namespace std;

class Numsys
{
    public:
		static string dec_to_hex(string n);
		static string hex_to_dec(string n);
		static string dec_to_oct(string n);
		static string oct_to_dec(string n);  
		static string dec_to_bin(string n);
		static string bin_to_dec(string n); 
};  

string Numsys::dec_to_hex(string n) {
	string hex;
	int num = atoi(n.c_str());
	while (num) {
		switch (num % 16) {
		case 10:
			hex.push_back('A');
			break;
		case 11:
			hex.push_back('B');
			break;
		case 12:
			hex.push_back('C');
			break;
		case 13:
			hex.push_back('D');
			break;
		case 14:
			hex.push_back('E');
			break;
		case 15:
			hex.push_back('F');
			break;
		default:
			hex.push_back(num % 16 + 48);
			break;
		}
		num /= 16;
	}
	reverse(hex.begin(), hex.end());
	return hex;
}

string Numsys::hex_to_dec(string n) {
	string dec;
	char tmp;
	int s = n.length(), d, p;
	for (d = 0, p = 0; s > 0; p++, s--) {
		tmp = n[s - 1];
		switch (tmp) {
		case 'A':
			d += 10 * pow(16, p);
			break;
		case 'B':
			d += 11 * pow(16, p);
			break;
		case 'C':
			d += 12 * pow(16, p);
			break;
		case 'D':
			d += 13 * pow(16, p);
			break;
		case 'E':
			d += 14 * pow(16, p);
			break;
		case 'F':
			d += 15 * pow(16, p);
			break;
		default:
			d += atoi(&tmp) * pow(16, p);
			break;
		}
	}
	dec = std::to_string(d);
	return dec;
}

string Numsys::dec_to_oct(string n) {
	string oct;
	for (int num = atoi(n.c_str()); num; num /= 8)
		oct.push_back(num % 8 + 48);

	reverse(oct.begin(), oct.end());
	return oct;
}

string Numsys::oct_to_dec(string n) {
	size_t s = n.length(), d = 0, p = 0;
	for (string tmp; s; s--, p++) {
		tmp = n[s - 1];
		d += atoi(tmp.c_str()) * pow(8, p);
	}
	return std::to_string(d);
}

string Numsys::dec_to_bin(string n) {
	string bin;
	for (int num = atoi(n.c_str()); num; num /= 2)
		bin.push_back(num % 2 + 48);

	reverse(bin.begin(), bin.end());
	return bin;
}

string Numsys::bin_to_dec(string n) {
	size_t s = n.length(), d = 0, p = 0;
	for (string tmp; s; s--, p++) {
		tmp = n[s - 1];
		d += atoi(tmp.c_str()) * pow(2, p);
	}
	return std::to_string(d);
}
